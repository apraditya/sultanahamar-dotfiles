local notify = require("notify")
notify.setup{
  background_colour = "#000000",
  minimum_width = 50,
}
vim.notify = notify
