-- Load custom tree-sitter grammar for org filetype
require('orgmode').setup_ts_grammar()

require('orgmode').setup({
  org_agenda_files = {'~/Downloads/notes/org/**/*'},
  org_default_notes_file = '~/Downloads/notes/org/refile.org',
})
