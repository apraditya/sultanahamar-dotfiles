syntax on

set guicursor=
set noshowmatch
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set termguicolors
set scrolloff=8
set ignorecase
set autoread "autoreads file if changed by other editor
" Give more space for displaying messages.
set cmdheight=2
set noequalalways " New vim windows created won't make everything back to same sizes
set spell
set spelllang=en_us " use z= to get word suggestions from vim. zg to add a word to dictionary and zw will mark word as wrong

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=50

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c
" Helps give good messages from LSP, without this, file info won't be print
set shortmess-=F

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey


call plug#begin('~/.config/nvim/plugged')

Plug 'christoomey/vim-tmux-navigator'

" fugitive - git integration
Plug 'tpope/vim-fugitive'
Plug 'lewis6991/gitsigns.nvim' " git gutter

" tpopes
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'

Plug 'kyazdani42/nvim-web-devicons'

" buffers as tabs
Plug 'jose-elias-alvarez/buftabline.nvim'

" Telescope file finder / picker, two above it are dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'stevearc/dressing.nvim'

" Project management 
Plug 'nvim-telescope/telescope-project.nvim'

" neovim language things
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'j-hui/fidget.nvim'
" shows and highlights signature as you type
Plug 'ray-x/lsp_signature.nvim'

Plug 'creativenull/diagnosticls-configs-nvim', { 'tag': 'v0.1.7' }
Plug 'norcalli/nvim-colorizer.lua'
" colors for brances, tags etc
Plug 'p00f/nvim-ts-rainbow' 

" Auto completion
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp'

" Snippets
Plug 'rafamadriz/friendly-snippets'

" For vsnip user.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
" Auto completion end

" For scala
Plug 'scalameta/nvim-metals'
" For android
Plug 'hsanson/vim-android'
Plug 'vim-test/vim-test'
Plug 'rcarriga/vim-ultest', { 'do': ':UpdateRemotePlugins' }

" For ruby debugging
Plug 'suketa/nvim-dap-ruby'

" Debugger
Plug 'mfussenegger/nvim-dap'
Plug 'rcarriga/nvim-dap-ui'
Plug 'theHamsta/nvim-dap-virtual-text' " cannot see virtual text if Treesitter for a language is not installed
Plug 'Pocco81/DAPInstall.nvim', { 'branch': 'dev' }
Plug 'nvim-telescope/telescope-dap.nvim'
" Debugger end

" File browser with git indicators
Plug 'kyazdani42/nvim-tree.lua'

Plug 'mbbill/undotree'

" indent indicators and lot of mini plugins
" Plug 'echasnovski/mini.nvim', { 'branch': 'stable' }
Plug 'echasnovski/mini.nvim' " use stable branch from above line once indent makes to master

" guess the indentation of file being edited and adopt
Plug 'nmac427/guess-indent.nvim'
" commenting out code
Plug 'tomtom/tcomment_vim'

" auto pairs closing braces on open etc
Plug 'windwp/nvim-autopairs'

Plug 'rcarriga/nvim-notify'
" all coloring below
" Plug 'gruvbox-community/gruvbox'
Plug 'EdenEast/nightfox.nvim'
" creates the status bar line showing git and other status
Plug 'nvim-lualine/lualine.nvim'

"godot completion and syntax highlighting
Plug 'calviken/vim-gdscript3'
"
"vim notes
Plug 'xolox/vim-notes'
Plug 'xolox/vim-misc'

" auto save
Plug '907th/vim-auto-save'


" Clojure
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'liquidz/vim-iced', {'for': 'clojure'}
Plug 'ggandor/lightspeed.nvim'

" Lua scratchpad with eval
Plug 'rafcamlet/nvim-luapad'
Plug 'weilbith/nvim-floating-tag-preview' "Helps preview ctags under cursor

Plug 'nvim-orgmode/orgmode'
call plug#end()

" Enable vim-iced's default key mapping
" This is recommended for newbies
let g:iced_enable_default_key_mappings = v:true
let g:iced_default_key_mapping_leader = '<LocalLeader>'

" let g:gruvbox_contrast_dark = 'medium'
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
" let g:gruvbox_invert_selection='0'

" vim notes specific configuration
let g:notes_smart_quotes = 0
let g:notes_directories = ['~/Downloads/notes/vim_notes']
" vim notes config ends here.

" vim auto save plugin config
let g:auto_save = 1  " enable AutoSave on Vim startup
"let g:auto_save_silent = 1  " do not display the auto-save notification
let g:auto_save_events = ["FocusLost", "TabLeave", "WinLeave", "BufLeave"]
" vim auto save plugin config ends

" --- vim game to practice movement
" let g:vim_be_good_floating = 0

" colorscheme gruvbox
" set background=dark

if executable('rg')
    let g:rg_derive_root='true'
endif
let loaded_matchparen = 1
let mapleader = " "
let maplocalleader = "\\"
let g:netrw_browse_split = 2
let g:netrw_liststyle = 0
let g:vrfr_rg = 'true'
let g:netrw_banner = 0
let g:netrw_winsize = 25

" copy, cut and paste
vmap <C-c> "+y

" move through buffers
nmap <leader>bN :bp!<CR>
nmap <leader>bn :bn!<CR>
nmap <leader>bd :bd<CR>
nmap <localleader>bd :%bd<CR>

nnoremap <leader>ps <cmd>lua require'telescope'.extensions.project.project{display_type = 'full'}<CR>

nnoremap <leader>u :UndotreeShow<CR>
autocmd User TelescopePreviewerLoaded setlocal wrap

" Lua plugins initialization
lua << EOF
require('nightfox')
vim.cmd('colorscheme duskfox')
require 'nvim-notify-setup'
require('mini.indentscope').setup()
require('mini.sessions').setup()
require('mini.starter').setup()
require 'lightspeed'.setup {
    ignore_case = true,
}
require 'orgmode_setup'
require('gitsigns_setup')
vim.cmd([[
  highlight FloatTitle guibg=none ctermbg=none
  highlight FloatBorder guibg=none guifg=grey ctermbg=none ctermfg=grey
]])
require 'dressing'.setup {
  input = {
    winblend = 0,
    winhighlight = "Normal:FloatTitle,NormalFloat:FloatTitle"
  }
}
require 'telescope_setup'
require'lsp_signature'.setup()
require"fidget".setup{
  timer = {
      task_decay = 5000,
      spinner_rate = 150,
  },
  text = {
    spinner = "moon"
  }
}
require'colorizer'.setup()
require('nvim-autopairs').setup{}
require'nvim-web-devicons'.setup {}
require("buftabline").setup { tab_format = " #{n}: #{i} #{b}#{f} "}
require('lualine').setup {
  sections = {
    lualine_c = {
      {
        'filename',
        file_status = true, -- displays file status (readonly status, modified status)
        path = 1 -- 0 = just filename, 1 = relative path, 2 = absolute path
      }
    }
  },
  options = {
    globalstatus = true
  }
}
require'nvim-tree'.setup {
  update_cwd = true
}

require("nvim-treesitter.configs").setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
    -- disable = {'org'}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
    additional_vim_regex_highlighting = {'org'}, -- Required since TS highlighter doesn't support all syntax features (conceal)
  },
  ensure_installed = {'org'}, -- Or run :TSUpdate org
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
  }
}
require('telescope').load_extension('dap')
require('telescope').load_extension('metals')

local dap_install = require("dap-install")

dap_install.setup({
  installation_path = vim.fn.stdpath("data") .. "/dapinstall/",
  verbosely_call_debuggers = true
})
require("dap_ui_setup");
local dbg_list = require("dap-install.api.debuggers").get_installed_debuggers()
for _, debugger in ipairs(dbg_list) do
  dap_install.config(debugger)
end
require("dap-ruby").setup()
require("nvim-dap-virtual-text").setup() -- cannot see virtual text if Treesitter for a language is not installed

require('guess-indent').setup {}
EOF

noremap <leader>pv :NvimTreeToggle<CR>
noremap <localleader>pv :NvimTreeFindFile<CR>

" gfiles shortcut
nnoremap <C-p> <cmd>Telescope git_files<cr>
nnoremap <Leader>pf <cmd>lua require"telescope.builtin".find_files({ hidden = true })<CR>
nnoremap <leader>bf <cmd>Telescope buffers<cr>

nnoremap <Leader>+ :vertical resize +5<CR>
noremap <Leader>- :vertical resize -5<CR>
nnoremap <Leader>rs :vertical resize 1000<CR>

vnoremap X "_d


" Sweet Sweet FuGITive
nmap <leader>ggj :diffget //3<CR>
nmap <leader>ggf :diffget //2<CR>
nmap <leader>gs :G<CR>

" my own bindings or mappings
nnoremap <leader>ptag <cmd>execute ':Ptag '.expand('<cword>')<cr>
nnoremap <localleader>F <cmd>lua require('telescope_setup').live_grep_in_glob()<cr>
nnoremap z= <cmd>lua require('telescope.builtin').spell_suggest()<cr>
nnoremap <leader>F <cmd>Telescope live_grep<cr>
nnoremap <leader>ht <cmd>Telescope help_tags<cr>
nnoremap <leader><Tab>  <cmd>lua require('telescope.builtin').oldfiles({cwd_only = true})<cr>
nnoremap <localleader><Tab>  <cmd>Telescope oldfiles<cr>

" copy current file path
nnoremap <leader>y <cmd>let @+ = expand("%")<cr>
nnoremap <leader>Y <cmd>let @+ = expand("%:p")<cr>
" navigate quickfix lix
nnoremap ]q <cmd>cn<cr>
nnoremap [q <cmd>cp<cr>

" for fuzzy search, do brew install the_silver_searcher
" for fuzzy search with regex, do brew install ripgrep
if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window (for an alternative on Windows, see simalt below).
  set lines=999 columns=999
  set guifont=Menlo\ Regular:h18
endif

" configure completion
set completeopt=menu,menuone,noselect
set complete+=k

" configure snippet expansion
imap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
smap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
imap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
smap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'

" DAP bindings or mappings
nnoremap <silent> <leader>dc :lua require'dap'.continue()<CR>
nnoremap <silent> <leader>do :lua require'dap'.step_over()<CR>
nnoremap <silent> <leader>di :lua require'dap'.step_into()<CR>
nnoremap <silent> <leader>dx :lua require'dap'.step_out()<CR>
nnoremap <silent> <leader>db :lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <leader>dB :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nnoremap <silent> <leader>dl :lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nnoremap <silent> <leader>dr :lua require'dap'.repl.open()<CR>
nnoremap <silent> <leader>drl :lua require'dap'.run_last()<CR>
nnoremap <silent> <leader>dev :lua require("dapui").eval()<CR>
vnoremap <silent> <leader>dev :lua require("dapui").eval()<CR>
nnoremap <silent> <leader>dclose :lua require("dapui").close()<CR>
nnoremap <silent> <leader>de :lua require("dap").close()<CR>

" Configure language server
" https://github.com/neovim/nvim-lspconfig#keybindings-and-completion
lua << EOF
local nvim_lsp = require('lspconfig')

  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      expand = function(args)
        -- For `vsnip` user.
        vim.fn["vsnip#anonymous"](args.body) 
      end,
    },
    mapping = {
      ['<C-u>'] = cmp.mapping.scroll_docs(-4),
      ['<C-d>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.close(),
      ['<CR>'] = cmp.mapping.confirm({ select = false, behavior = cmp.ConfirmBehavior.Replace, }),
      ['<c-n>'] = function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        else
          fallback()
        end
      end,
      ['<c-p>'] = function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        else
          fallback()
        end
      end

    },
    sources = {
      { name = 'nvim_lsp' },
      { name = 'orgmode' },

      -- For vsnip user.
      { name = 'vsnip' },

      { name = 'buffer' },
    }
  })
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({  map_char = { tex = '' } }))
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  -- lsp bindings
  buf_set_keymap('n', '<leader>gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', '<leader>gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
  buf_set_keymap('n', '<leader>gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<leader><CR>', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('v', '<leader><CR>', '<cmd>lua vim.lsp.buf.range_code_action()<CR>', opts)
  buf_set_keymap('n', '<leader>gr', "<cmd>lua require'telescope.builtin'.lsp_references()<CR>", opts)
  buf_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<cr>', opts)
  buf_set_keymap('n', '<leader>sym', '<cmd>Telescope lsp_document_symbols<cr>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next{severity = { min = vim.diagnostic.severity.HINT, max = vim.diagnostic.severity.WARN }}<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev{severity = { min = vim.diagnostic.severity.HINT, max = vim.diagnostic.severity.WARN }}<CR>', opts)
  buf_set_keymap('n', ']e', '<cmd>lua vim.diagnostic.goto_next{severity = { min = vim.diagnostic.severity.ERROR }}<CR>', opts)
  buf_set_keymap('n', '[e', '<cmd>lua vim.diagnostic.goto_prev{severity = { min = vim.diagnostic.severity.ERROR }}<CR>', opts)
  buf_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

  -- Set some keybinds conditional on server capabilities
  if client.resolved_capabilities.document_formatting then
    buf_set_keymap("n", "<leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
  elseif client.resolved_capabilities.document_range_formatting then
    buf_set_keymap("n", "<leader>f", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end
    buf_set_keymap("v", "<leader>f", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)

  buf_set_keymap("n", "<leader>clR", "<cmd>lua vim.lsp.codelens.refresh()<cr>", opts)
  buf_set_keymap("n", "<leader>clr", "<cmd>lua vim.lsp.codelens.run()<cr>", opts)
end
local capabilitiesWithCompletion = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

local lsp_installer = require("nvim-lsp-installer")

-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
lsp_installer.on_server_ready(function(server)
    local opts = {
       on_attach = on_attach,
       flags = {
         debounce_text_changes = 150,
       },
       capabilities = capabilitiesWithCompletion
      }
    server:setup(opts)
end)

local dlsconfig = require 'diagnosticls-configs'
dlsconfig.init {
  default_config = true,
  on_attach = on_attach,
}
local rubocop = require 'diagnosticls-configs.linters.rubocop'
dlsconfig.setup{
  ['ruby'] = {
      linter = rubocop,
    }
}
--  not using local here so that its available in vim autocmd later
metals_config = require("metals").bare_config()
metals_config.on_attach = function(client, bufnr)
  on_attach(client, bufnr)
  require("metals").setup_dap()
end
vim.cmd([[autocmd FileType scala,sbt lua require("metals").initialize_or_attach(metals_config)]])
EOF

" vim-test config
let test#strategy = "neovim"
let g:test#neovim#start_normal = 1
let g:test#java#gradletest#options = ' --info'
let test#java#runner = 'gradletest'
"vim-test bindings
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>
